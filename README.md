# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://gc49502@bitbucket.org/gc49502/stroboskop.git
cd stroboskop

```

Naloga 6.2.3:
https://bitbucket.org/gc49502/stroboskop/commits/4f5bec09dad286801a63d9d39bc87184d277829f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/gc49502/stroboskop/commits/ff06a97793535394bf58b93e8cbd5f1c71156ea2

Naloga 6.3.2:
https://bitbucket.org/gc49502/stroboskop/commits/b478db3de5a97d315fab68e98862da7f0073b29d

Naloga 6.3.3:
https://bitbucket.org/gc49502/stroboskop/commits/1f1aebfcdef23303989a9bfec8b33fe0c8d4a484

Naloga 6.3.4:
https://bitbucket.org/gc49502/stroboskop/commits/80d7f9c763539102eb4eecd2799512050d7e65c9

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/gc49502/stroboskop/commits/750d2b7985dcf551fdc533e33fd8af3c83ee9414

Naloga 6.4.2:
https://bitbucket.org/gc49502/stroboskop/commits/098e69734dc5ff36495cef8b1ed057275ca30116

Naloga 6.4.3:
https://bitbucket.org/gc49502/stroboskop/commits/111988c4d53d2615c07e84cf7cf79ac65966013d

Naloga 6.4.4:
https://bitbucket.org/gc49502/stroboskop/commits/b681ac710c45524a91c31a2fabd81332940ae6b2